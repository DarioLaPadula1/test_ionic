import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

/*
import { SERVER_URL } from '../environments/environment';
import { SERVER_PORT } from '../environments/environment';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
*/
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';

/*
const config: SocketIoConfig = { url: SERVER_URL + ':' + SERVER_PORT, options: { secure: true, transports: ['websocket'] } };
*/

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, /*SocketIoModule.forRoot(config), SocketIoModule.forRoot(config),*/ HttpClientModule,
      IonicStorageModule.forRoot(),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
