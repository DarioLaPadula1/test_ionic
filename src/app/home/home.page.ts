import {Component, OnInit} from '@angular/core';
import {Socket} from 'ng-socket-io';
import {ToastController} from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    logout() {
        this.auth.logout();
    }
}