/**
 * Ionic Full App  (https://store.enappd.com/product/ionic-full-app-ionic-4-full-app)
 *
 * Copyright © 2019-present Enappd. All rights reserved.
 *
 * This source code is licensed as per the terms found in the
 * LICENSE.md file in the root directory of this source tree.
 */
import {Component, OnInit} from '@angular/core';
import {MenuController, Platform, AlertController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {UtilService} from '../services/util/util.service';
import {LoadingController} from '@ionic/angular';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

    email = '';
    password = '';

    constructor(private platform: Platform,
                public loadingController: LoadingController,
                public alertController: AlertController,
                private splashScreen: SplashScreen,
                public util: UtilService,
                private menuCtrl: MenuController,
                private auth: AuthService,
                private router: Router) {
    }

    login() {
        this.auth.login({email: this.email, password: this.password}).subscribe(async res => {
            if (res.success) {
                this.auth.addTokenKey(res.token);
                this.router.navigateByUrl('/members');
            } else {
                const alert = await this.alertCtrl.create({
                    header: 'Login Failed',
                    message: 'Wrong credentials.',
                    buttons: ['OK']
                });
                await alert.present();
            }
        });
    }

    ngOnInit() {

    }

    ionViewDidEnter() {
        this.menuCtrl.enable(true, 'start');
        this.menuCtrl.enable(true, 'end');
    }
}
