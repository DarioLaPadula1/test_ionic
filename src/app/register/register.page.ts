/**
* Ionic Full App  (https://store.enappd.com/product/ionic-full-app-ionic-4-full-app)
*
* Copyright © 2019-present Enappd. All rights reserved.
*
* This source code is licensed as per the terms found in the
* LICENSE.md file in the root directory of this source tree.
*/

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, MenuController, Platform } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    credentialsForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private router: Router, private menuCtrl: MenuController, private auth: AuthService,
              public alertCtrl: AlertController, private platform: Platform) { }

    ngOnInit() {
        this.credentialsForm = this.formBuilder.group({
            name: ['', [Validators.required]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(5)]],
            password_confirmation: ['', [Validators.required, Validators.minLength(5)]]
        });

        console.log(this.credentialsForm.errors);
    }

    register() {
        this.auth.register(this.credentialsForm.value).subscribe(async res => {
            if (res.success) {
                let alert = await this.alertCtrl.create({
                    header: 'Subscription success',
                    message: 'Mail Sent.',
                    buttons: ['OK']
                });
                alert.present();

            } else {
                this.auth.showAlert('Wrong Data');
            }

        });
    }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'start');
    this.menuCtrl.enable(true, 'end');
  }

}
