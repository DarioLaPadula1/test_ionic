import { Injectable } from "@angular/core";
import {Headers, Http} from '@angular/http';
import {AuthHttp} from 'angular2-jwt';

import 'rxjs/add/operator/toPromise';
import {Environment} from '../../environments';

@Injectable()
export class ApiService {

  url:string;
  constructor(
    public http: Http,
    private authHttp: AuthHttp
  ) {

  }

  get(id_token, url): Promise<any> {

    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/json');
    myHeader.append( 'Authorization', "Bearer \\(" + id_token + ")" );

    return this
      .authHttp.get(url,{
        headers: myHeader
      })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);

  }

  getWithParams(id_token, url, params): Promise<any> {

    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/json');
    myHeader.append( 'Authorization', "Bearer \\(" + id_token + ")" );

    return this
      .authHttp.get(url,{
        params: params,
        headers: myHeader
      })
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);

  }

  post(id_token, url, params): Promise<any>{

    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/json');
    myHeader.append( 'Authorization', "Bearer \\(" + id_token + ")" );

    return this.http.post(
      url,
      JSON.stringify(params),
      { headers: myHeader }
    )
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);

  }

  private handleError(error: any): Promise<any> {

    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);

  }

}
