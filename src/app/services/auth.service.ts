import {Platform, AlertController} from '@ionic/angular';
import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';
import {BehaviorSubject, Observable, from, of} from 'rxjs';
import {map, switchMap, catchError} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Environment} from '../../environments';
import {REMOTE_URL} from '../../environments/environment';
import {stringify} from 'querystring';

const helper = new JwtHelperService();
const TOKEN_KEY = 'token';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public user: Observable<any>;
    private userData = new BehaviorSubject(null);
    private options = {
        headers: new HttpHeaders()
            .append('Content-Type', 'application/json; charset=utf-8')
            .append('Access-Control-Allow-Origin', '*')
            .append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT')
            .append('Accept', 'application/json'),
        params: new HttpParams()
    };

    constructor(private storage: Storage, private http: HttpClient, private plt: Platform, private router: Router, private alertController: AlertController) {

    }

    public isAuthenticated(): boolean {
        // Get token from localStorage
        const token = this.getToken();
            // Check if token is null or empty
            if (token) {
                // Check whether the token is expired and return
                // true or false
                return !helper.isTokenExpired(token);
            }
            return false;
    }

    getToken() {
        return localStorage.getItem(TOKEN_KEY);
        /*let platformObs = from(this.plt.ready());

        this.user = platformObs.pipe(
            switchMap(() => {
                return from(localStorage.getItem(TOKEN_KEY));
            }),
            map(token => {
                if (token) {
                    let decoded = helper.decodeToken(token);
                    //this.userData.next(decoded);
                    return true;
                } else {
                    return null;
                }
            })
        );*/
    }

    login(credentials) {
        return this.http.post(REMOTE_URL + 'login', JSON.stringify(credentials), this.options).pipe(
            catchError(e => {
                let status = e.status;
                if (status === 401) {
                    this.showAlert('You are not authorized for this!');
                    this.logout();
                }
                throw new Error(e);
            })
        );
    }

    register(credentials) {
        return this.http.post(REMOTE_URL + 'register', JSON.stringify(credentials), this.options).pipe(
            catchError(e => {
                let status = e.status;
                if (status === 401 || status === 422) {
                    this.showAlert('Wrong data');
                    //this.logout();
                }
                throw new Error(e);
            })
    );
    }


    lostPassword(credentials) {
        return this.http.post(this.LOST_PASSWORD_URL, JSON.stringify(credentials), this.options)
            .map(res => res.json());
    }

    /*login(email: string, password: string) {


        return this.http.post(REMOTE_URL + 'login', {email, password}, options).pipe(
            map(res => {
                console.log(res.success);
                if (res.success) {
                    this.userData.next(res.name);
                    return res.token;
                }
            }),
            switchMap(token => {
                let decoded = helper.decodeToken(token);
                //this.userData.next(decoded);

                let storageObs = from(localStorage.setItem(TOKEN_KEY, token));
                return storageObs;
            }),
            catchError(e => {
                let status = e.status;
                if (status === 401) {
                    this.showAlert('You are not authorized for this!');
                    this.logout();
                }
                throw new Error(e);
            }),

        );
    }*/
/*
    getUser() {
        return this.userData.getValue();
    }*/

    addTokenKey(token) {
        localStorage.setItem(TOKEN_KEY, token);
    }

    logout() {
        localStorage.removeItem(TOKEN_KEY);
            this.router.navigateByUrl('/');
//            this.userData.next(null);

    }

    showAlert(msg) {
        let alert = this.alertController.create({
            message: msg,
            header: 'Error',
            buttons: ['OK']
        });
        alert.then(alert => alert.present());
    }

}
