import {Component} from '@angular/core';
import {Echo} from 'laravel-echo-ionic';

@Component({
    selector: 'app-test',
    templateUrl: 'test.page.html'
})
export class TestPage {
    echo: any = null;

    constructor() {
        this.echo = new Echo({
            broadcaster: 'socket.io',
            host: 'localhost:6001',
        });

        this.echo.connector.socket.on('connect', function () {
            console.log('CONNECTED');
        });

        this.echo.connector.socket.on('reconnecting', function () {
            console.log('CONNECTING');
        });

        this.echo.connector.socket.on('disconnect', function () {
            console.log('DISCONNECTED');
        });

        this.echo.channel('chat')
            .listen('test', (data) => {
                console.log(1);
                console.log(data);
            });

        this.echo.channel('chat')
            .listen('.test', (data) => {
                console.log(data);
            });
    }
}
