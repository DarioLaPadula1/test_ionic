export const environment = {
  production: true
};

export const SERVER_URL = '127.0.0.1';
export const SERVER_PORT = '5011';